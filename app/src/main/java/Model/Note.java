package Model;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Note {

    private String title;
    private String content;
    public String key;
    public String date;
public Note(){

}

    public Note(String t, String c, String key,String date){
        this.title = t;
        this.content = c;
        this.key = key;
        this.date = date;
    }

    public String getTitle(){
        return this.title;
    }
    public String getContent(){
        return this.content;
    }
    public void setTitle(String newTitle){
    title = newTitle;
    }
    public void setKey(String newKey){
        key = newKey;
    }
    public void setContent(String newContent){
        content = newContent;
    }
    public Map<String,Object> toMap(){
        HashMap<String,Object> result = new HashMap<>();
        result.put("title",title);
        result.put("content",content);
        result.put("key",key);
        result.put("date",date);

        return result;
    }


}
