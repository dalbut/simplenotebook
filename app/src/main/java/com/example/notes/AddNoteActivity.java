package com.example.notes;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import Model.Note;

public class AddNoteActivity extends AppCompatActivity {


    private FirebaseDatabase database;
    private DatabaseReference reference;
    private Button saveNote;
    private EditText first;
    private EditText second;
    private FirebaseAuth mAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        reference = database.getReference("Notes");
        saveNote = findViewById(R.id.save_note);
        first = findViewById(R.id.note_title_inpt);
        second = findViewById(R.id.note_content_inpt);
        saveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNote();
                startActivity(new Intent(AddNoteActivity.this,ProfileActivity.class));
            }
        });


    }

    private void addNote(){
        String id = reference.push().getKey();

        //String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        String mydate = java.text.DateFormat.getDateInstance().format(Calendar.getInstance().getTime());

        Note newNote = new Note(first.getText().toString(),second.getText().toString(),id,mydate.toString());
        Map<String,Object> noteValues = newNote.toMap();
        Map<String, Object> note = new HashMap<>();
        note.put(id,noteValues);
        String path = mAuth.getCurrentUser().getEmail().replace('.',',');
        //reference.child(path).setValue(note);
        reference.child(path).updateChildren(note);
      //  reference.updateChildren(note);
    }


}
