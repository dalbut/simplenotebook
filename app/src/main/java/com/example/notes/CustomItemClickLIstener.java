package com.example.notes;

import android.view.View;

interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}
