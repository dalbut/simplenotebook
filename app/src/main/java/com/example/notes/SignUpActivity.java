package com.example.notes;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import Model.User;

public class SignUpActivity extends AppCompatActivity {


    Toolbar toolBar;
    ProgressBar progressBar;
    EditText email;
    EditText password;
    Button signUp;
   // Button logIn;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("Notes");

        toolBar = findViewById(R.id.toolbar);
        progressBar = findViewById(R.id.progressBarR);
        email = findViewById(R.id.etEmailR);
        password = findViewById(R.id.etPasswordR);
        signUp = findViewById(R.id.SignUpR);
       // logIn = findViewById(R.id.LogIn);
        toolBar.setTitle(R.string.app_name);



        signUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                String eml = email.getText().toString().trim();
                String pwd = password.getText().toString().trim();



                if (TextUtils.isEmpty(eml)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                if (TextUtils.isEmpty(pwd)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                if (pwd.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    password.setText("");
                    return;
                }

                mAuth.createUserWithEmailAndPassword(eml, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            Toast.makeText(SignUpActivity.this, "Registered succesfully", Toast.LENGTH_LONG).show();
                           // String path = "Notes/" + email.getText().toString().replace('.',',');
                          //  reference.push().child(path);

                            startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
                        } else {
                            Toast.makeText(SignUpActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            password.setText("");
                            email.setText("");
                        }
                    }
                });
            }
        });

    }
}
