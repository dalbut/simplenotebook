package com.example.notes;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import Model.Note;


public class ProfileActivity extends AppCompatActivity {

    TextView welcome;
    TextView welcome2;
    FirebaseUser user;
    FirebaseAuth auth;
    private static final String TAG = "MyActivity";
private RecyclerView recyclerView;
private List<Note> result;
private NoteAdapter adapter;
private TextView no_data;
    private FirebaseAuth mAuth;
private FirebaseDatabase database;
private DatabaseReference reference;
    ImageView add_note;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        welcome = findViewById(R.id.etwelcometxt);
        add_note = (ImageView) findViewById(R.id.add_notebtn);
        mAuth = FirebaseAuth.getInstance();

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        assert user != null;
        welcome.setText(user.getEmail());
        no_data = findViewById(R.id.no_items);

        database = FirebaseDatabase.getInstance();
        String path ="Notes/"+ mAuth.getCurrentUser().getEmail().replace('.',',');
        reference = database.getReference(path);

        result = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.note_list);
        LinearLayoutManager lin = new LinearLayoutManager(this);
        lin.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(lin);


        adapter = new NoteAdapter(result, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
               // Log.d(TAG, "clicked position:" + position);
                int index = getNoteIntex(result.get(position));
                removeUser(position);
            }
        });
        recyclerView.setAdapter(adapter);


        updateList();
        checkIsEmpty();




        add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this,AddNoteActivity.class));
            }
        });


    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 0:
                removeUser(item.getGroupId());
            case 1:
                changeUser(item.getGroupId());
                break;
        }

        return super.onContextItemSelected(item);
    }



    private void updateList(){
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                result.add(dataSnapshot.getValue(Note.class));
                adapter.notifyDataSetChanged();
                checkIsEmpty();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Note note = dataSnapshot.getValue(Note.class);

                int index = getNoteIntex(note);

                result.set(index,note);
                adapter.notifyItemChanged(index);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                Note note = dataSnapshot.getValue(Note.class);
                int index = getNoteIntex(note);
                result.remove(index);
                adapter.notifyItemRemoved(index);
                checkIsEmpty();
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private int getNoteIntex(Note note){
        int index = -1;
        for(int i = 0; i < result.size();i++){
            if(result.get(i).key.equals(note.key)){
                index = i;
                break;
            }
        }
        return index;
    }
    private void removeUser(int position){
        reference.child(result.get(position).key).removeValue();


    }
    private void checkIsEmpty(){
        if(result.size() == 0){
            recyclerView.setVisibility(View.INVISIBLE);
            no_data.setVisibility(View.VISIBLE);
        }else{
            recyclerView.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.INVISIBLE);
        }
    }
    private void changeUser(int position){
        Note note = result.get(position);
        Random r = new Random();
        r.nextInt();
        String a = r.toString();

        note.setTitle(a);
        note.setContent(a);
        note.setKey(a);

        Map<String,Object> noteValues = note.toMap();
        Map<String, Object> newNote = new HashMap<>();

        newNote.put(note.key,noteValues);
        reference.updateChildren(newNote);
    }
}



