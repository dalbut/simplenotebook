package com.example.notes;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import Model.Note;



public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder>{

    public NoteAdapter(List<Note> list,CustomItemClickListener listener) {
        this.list = list;
        this.asd = listener;
    }

    private List<Note> list;
    CustomItemClickListener asd;



    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View mView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item,viewGroup,false);
        final NoteViewHolder mViewHolder = new NoteViewHolder(mView);
        mViewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asd.onItemClick(v,mViewHolder.getAdapterPosition());
            }
        });

        return mViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull final NoteViewHolder noteViewHolder, int i) {
        Note note = list.get(i);
        noteViewHolder.content_title.setText(note.getTitle());
        noteViewHolder.content_note.setText(note.getContent());
        noteViewHolder.content_date.setText(note.date);



        noteViewHolder.itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
               // menu.add(noteViewHolder.getAdapterPosition(),0,0,"Delete");
              //  menu.add(noteViewHolder.getAdapterPosition(),1,0,"Edit");
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class NoteViewHolder extends RecyclerView.ViewHolder{
        TextView content_title,content_note,content_date;
        ImageView remove;

        public NoteViewHolder(View itemView){
            super(itemView);

            content_title = (TextView) itemView.findViewById(R.id.title_note);
            content_note = (TextView) itemView.findViewById(R.id.content_note);
            remove = (ImageView) itemView.findViewById(R.id.remove_note);
            content_date = (TextView) itemView.findViewById(R.id.note_date);

        }
    }
}
